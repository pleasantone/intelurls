import unittest
import json

import intelurls

class TestSequence(unittest.TestCase):
    pass

def testgen_is_map(a, b):
    def test(self):
        self.assertEqual(intelurls.check_mapurl(a), b)
    return test

def testgen_parse_map(a, b):
    def test(self):
        self.assertDictEqual(intelurls.parse_mapurl(a), b)
    return test

def testgen_parse_location(a, b):
    def test(self):
        self.assertDictEqual(intelurls.parse_natural(a), b)
    return test

with open("tests/valid.out", "r") as stream:
    tests = json.load(stream)

for i, t in enumerate(tests["is_map"]):
    test_name = 'test_is_map_{}'.format(i)
    test = testgen_is_map(t[0], t[1])
    setattr(TestSequence, test_name, test)

for i,t in enumerate(tests["parse_map"]):
    test_name = 'test_parse_map_{}'.format(i)
    test = testgen_parse_map(t[0], t[1])
    setattr(TestSequence, test_name, test)

for i,t in enumerate(tests["parse_location"]):
    test_name = 'test_parse_location_{}'.format(i)
    test = testgen_parse_location(t[0], t[1])
    setattr(TestSequence, test_name, test)

if __name__ == '__main__':
    unittest.main()
